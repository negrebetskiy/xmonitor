#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <sys/poll.h>
#include <X11/Xlib.h>
#include <X11/Xft/Xft.h>
#include <X11/extensions/Xrandr.h>

#define MAXCH 8192
#define CLERR 5 

/* Globals */
Display *dpy;
int screen;
Window win;
Visual *visual;
Colormap cmap;
XftDraw *xdraw;
XftFont *xfont;
XftColor xcol;

char text[MAXCH] = {0}, buff[MAXCH] = {0};
int *xpos, *ypos;
int nmons;
int tpos, bpos;
int cw, ch;
int done = 0;
int dbg = 0;

/* Prototypes */
void finisher();
void help(FILE *out);
int initsig();
int initX();
int initmons(const int x, const int y, const int mnum);
int setcol(const char *color);
int setfont(const char *font);
int readstream(FILE *fd, const char* delim, int *upd);
char *findline(const char* src, int *pos, char *dest); 
int redraw(const int sp);
int loop(const char *cmd, const char *delim, const int sp);

/* Functions */

void
finisher(int signal) 
{
  done = 1;
  return;
}

void
help(FILE* out)
{
  fprintf(out, "usage: xmonitor [-h] [-C cmd] [-f font] [-c color]\n"
               "                [-x|y pos] [-s space] [-m monitor]\n"
               "                [-d delimeter] [-D]\n");
}

int 
initsig()
{
  struct sigaction sa = {0};
  memset(&sa, 0, sizeof(struct sigaction));
  sa.sa_handler = finisher;
  if (sigaction(SIGINT, &sa, NULL) || sigaction(SIGTERM, &sa, NULL))
  {
    fprintf(stderr, "Failed to set interuption handler.\n");
    return 1;
  }
  return 0;
}

int
initX()
{
  if (!(dpy = XOpenDisplay(NULL)))
  {
    fprintf(stderr, "Failed to start X session.\n");
    return 1;
  }
  screen = DefaultScreen(dpy);
  win = RootWindow(dpy, screen);
  XClearWindow(dpy, win);
  XMapWindow(dpy, win);
  XSelectInput(dpy, win, ExposureMask);
  
  XWindowAttributes wa;
  XGetWindowAttributes(dpy, win, &wa);
  cmap = wa.colormap;
  visual = wa.visual;
  xdraw = XftDrawCreate(dpy, win, visual, cmap); 
  return 0;
}

int 
initmons(const int x, const int y, const int mnum) {
  XRRMonitorInfo *mons = XRRGetMonitors(dpy, win, True, &nmons);
  if (nmons == -1 || nmons <= mnum) {
    fprintf(stderr, "Failed to get monitors.\n");
    return 1; 
  }
 
  nmons = mnum == -1 ? nmons : 1;
  xpos = malloc(nmons * sizeof(int));
  ypos = malloc(nmons * sizeof(int)); 
  for (int m = 0; m < nmons; m++) {
    xpos[m] = mnum == -1 ? mons[m].x : mons[mnum].x;
    ypos[m] = mnum == -1 ? mons[m].y : mons[mnum].y;
    ypos[m] += y;
    xpos[m] += x;
  }
  return 0;
}

int
setcol(const char *color)
{
  if (!XftColorAllocName(dpy, visual, cmap, color, &xcol))
  {
    fprintf(stderr, "Failed to allocate color \"%s\".\n", color);
    return 1;
  }
  return 0;
}

int
setfont(const char *font)
{
  xfont = XftFontOpenName(dpy, screen, font);
  // TODO: this check won't help for some reason!
  if (!xfont)
  {
    fprintf(stderr, "Failed to load font \"%s\".\n", font);
    return 1;
  }
  return 0;
}

int 
readstream(FILE *fd, const char *delim, int *upd) {
  char line[MAXCH];
  int llen;
  /* Read line */
  if (NULL == fgets(line, MAXCH, fd)) {
    fprintf(stderr, "Failed to read from stream.\n");
    return 1;
  }
  llen = strlen(line);

  /* Found a brake at the end */
  if (line[llen - 1] == '\n') {
    line[--llen] = '\0';  
  }
  
  /* Line is too long */
  if (llen >= MAXCH - bpos - 2) {
    line[MAXCH - bpos - 2] = '\0';
    llen = strlen(line);
  }
  
  if (strcmp(delim, line)) { // regular line
    line[llen++] = '\n';
    for (int i = 0; i < llen; i++)
      buff[bpos + i] = line[i];
    bpos += llen;
    *upd = 0;
  }
  else { // found delimeter
    buff[bpos++] = '\0';
    for (int i = 0; i < bpos; i++)
      text[i] = buff[i];
    tpos = bpos;
    bpos = 0;
    *upd = 1;
  }
  return 0;
}

char*
findline(const char *src, int *pos, char *dest) {
  int start = *pos;
  
  for (; *pos < strlen(src); (*pos)++) {
    if (src[*pos] == '\n') {
      break;
    }
  }

  dest = realloc(dest, (*pos - start + 1) * sizeof(char));
  dest[*pos - start] = '\0';
  dest = realloc(dest, (*pos - start) * sizeof(char));
  for (int i = 0; i < *pos - start; i++) {
    dest[i] = src[start + i];
  }
  (*pos)++;
  
  return dest;
}

int
redraw(const int sp) {
  int nlines = 0, pos = 0;
  char *str = NULL;
  const int fh = xfont->height;

  /* Cleaning */
  for (int m = 0; m < nmons; m++) {
    XClearArea(dpy, win, xpos[m] - CLERR, ypos[m] - CLERR, 
      cw + 2 * CLERR, fh + ch + 2 * CLERR, 0);
  }
  
  /* Draw stuff and update area sizes */
  cw = 0;
  while ((str = findline(text, &pos, str)) != NULL) {
    int width = strlen(str) * xfont->max_advance_width;
    cw = width > cw ? width : cw;
    nlines++;

    for (int m = 0; m < nmons; m++) {
      XftDrawStringUtf8(xdraw, &xcol, xfont, 
        xpos[m], ypos[m] + nlines * (fh + sp) - sp,
        (const FcChar8 *)str, strlen(str));
    } 
  }
  ch = nlines * fh + (nlines - 1) * sp;
  
  /* Draw outline */
  if (dbg) {
    for (int m = 0; m < nmons; m++) {
      int delta = CLERR - 2;
      int dy = fh + ch + 2 * delta;
      int dx = cw + 2 * delta;
      int x0 = xpos[m] - delta;
      int y0 = ypos[m] - delta;
      XftDrawRect(xdraw, &xcol, x0, y0, 1, dy);
      XftDrawRect(xdraw, &xcol, x0 + dx, y0, 1, dy);
      XftDrawRect(xdraw, &xcol, x0, y0, dx, 1);
      XftDrawRect(xdraw, &xcol, x0, y0 + dy, dx, 1);
    }
  }

  XFlush(dpy);
  return 0;
}

int 
loop(const char *cmd, const char *delim, const int sp) {
  int need_update = 0;
  nfds_t nfds = 2;
  XEvent ev;
  
  /* Prepare descriptors */
  FILE *stream = popen(cmd, "r");
  struct pollfd fds[] = {
    {.fd = ConnectionNumber(dpy), .events = POLLIN},
    {.fd = fileno(stream),        .events = POLLIN},
  };
  
  tpos = bpos = 0;
  while(!done) {
    poll(fds, nfds, -1); // wait for event
    /* X11 event */
    if (fds[0].revents & POLLIN) {
      XNextEvent(dpy, &ev);
      if (ev.type == Expose && ev.xexpose.count == 0) {
        need_update = 1;
      }
    }
    /* Stream event */
    if (fds[1].revents & POLLIN) {
      if (readstream(stream, delim, &need_update)) {
        return 1;
      }
    }
    /* Draw */
    if (need_update) {
      redraw(sp);
    }
  }

  pclose(stream); 
  return 0;
}

int
main(int argc, char **argv)
{
  int result = 1;

  /* Defaults */
  char *font = "monospace";
  char *color = "#ff0000";
  char *cmd = "while :; do date; whoami; whoami; echo update; sleep 1; done";
  int x = 50, y = 50;
  int space = 5;
  int mnum = -1;
  char *delimeter = "update";
  
  /* Parse user arguments */
  int opt = 0;
  while ((opt = getopt(argc, argv, "hC:f:c:x:y:s:m:d:D")) != -1) {
    switch (opt) {
      case 'h': help(stdout); goto Exit;
      case 'C': cmd = optarg; break;
      case 'f': font = optarg; break;
      case 'c': color = optarg; break;
      case 'x': x = atoi(optarg); break;
      case 'y': y = atoi(optarg); break;
      case 's': space = atoi(optarg); break;
      case 'm': mnum = atoi(optarg); break;
      case 'd': delimeter = optarg; break;
      case 'D': dbg = 1; break;
      default: help(stderr); goto Exit;
    }
  }
  if (strlen(delimeter) >= MAXCH) { 
    fprintf(stderr, "Delimeter is too long.\n");
    goto Exit;
  }
  
  /* Initialize */
  if (initsig() || initX() || initmons(x, y, mnum)) {
    goto Exit;
  }
  setfont(font);
  setcol(color);
  
  /* Main loop */
  result = loop(cmd, delimeter, space); 

Exit:
  /* Clean */
  if (xpos) free(xpos);
  if (ypos) free(ypos);
  if (win) XClearWindow(dpy, win);
  if (dpy) XFlush(dpy);
  return result;
}

