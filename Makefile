CC = cc
CFLAGS = -std=c99 -Wall -pedantic -Os -D_POSIX_C_SOURCE=200809L
LIBS = -lX11 -lXft -lfontconfig -lXrandr
INCS = -I/usr/include/freetype2

all: xmonitor

xmonitor: xmonitor.c
	${CC} ${CFLAGS} ${LIBS} ${INCS} $< -o $@

clean:
	rm -f xmonitor

.PHONY: all clean
